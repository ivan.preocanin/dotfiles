ln -s ~/workspace/dotfiles/.zshrc ~/.zshrc

ln -s ~/workspace/dotfiles/.p10k.zsh ~/.p10k.zsh

ln -s ~/workspace/dotfiles/.vimrc ~/.vimrc

ln -s ~/workspace/dotfiles/.gitconfig ~/.gitconfig

mkdir -p ~/.config/sway/
ln -s ~/workspace/dotfiles/.config/sway/config ~/.config/sway/config

mkdir -p ~/.config/alacritty/
ln -s ~/workspace/dotfiles/.config/alacritty/alacritty.toml ~/.config/alacritty/alacritty.toml

mkdir -p ~/.config/nvim/
ln -s ~/workspace/dotfiles/.config/nvim ~/.config/nvim

mkdir -p ~/.config/zathura/
ln -s ~/workspace/dotfiles/.config/zathura/zathurarc ~/.config/zathura/zathurarc

mkdir -p ~/.config/vifm/
ln -s ~/workspace/dotfiles/.config/vifm ~/.config/vifm

