-- https://github.com/nanotee/nvim-lua-guide

-- Disable netrw due to race conditions and conflicts with nvim-tree
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require "user.options"
require "user.keymaps"
require "user.lazy"
require "user.colorschemes.gruvbox"
require "user.cmp"
require "user.mason"
require "user.mason-lspconfig"
require "user.lspconfig"
require "user.nvim-tree"
require "user.autocommands"
require "user.treesitter"
require "user.autopairs"
require "user.gitsigns"
require "user.null-ls"
require "user.lualine"
