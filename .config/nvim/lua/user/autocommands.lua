-- Language indents and line lengths
-- autocmd FileType py,rst,md   set textwidth=79 -- I don't need this atm, but I might need it some day

-- autocmd FileType tf  set softtabstop=2 shiftwidth=2
-- vim.api.nvim_create_autocmd('FileType', { command = 'tf set softtabstop=2 shiftwidth=2' })

-- autocmd FileType typescript setlocal commentstring=//\ %s
-- vim.api.nvim_create_autocmd('FileType', { command = 'typescript setlocal commentstring=// %s' })

-- TODO: vim.api.winnr('$')
vim.api.nvim_create_autocmd('BufEnter', { command = 'if (winnr("$") == 1 && &filetype == "NvimTree") | q | endif' })
