vim.cmd 'colorscheme NeoSolarized'
vim.opt.background = 'dark'
vim.g.airline_theme = 'base16_solarized_dark'

