local configs = require('nvim-treesitter.configs')

local parser_install_dir = vim.fn.stdpath("cache") .. "treesitters"
vim.fn.mkdir(parser_install_dir, "p")
vim.opt.runtimepath:append(parser_install_dir)

configs.setup {
    autopairs = {
        enable = true,
    },
    auto_install = true,
    ensure_installed = {
        'bash',
        'css',
        'dockerfile',
        'elm',
        'git_rebase',
        'html',
        'javascript',
        'latex',
        'lua',
        'python',
        'regex',
        'rust',
        'toml',
        'typescript',
        'yaml',
        'rust',
    },
    sync_install = false, -- install languages synchronously (only applied to 'ensure_installed')
    highlight = {
        enable = true, -- false will disable the whole extension
        additional_vim_regex_highlighting = false,
    },
    parser_install_dir = parser_install_dir,
}
