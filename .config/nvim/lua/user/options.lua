local options = {
    -- syntax = 'on',
    guicursor = '',
    termguicolors = true,
    number = true,
    relativenumber = true,
    wrap = false,
    linebreak = true,
    smartcase = true,
    incsearch = true,
    timeoutlen = 150,
    scrolloff = 8,
    showmode = false,
    clipboard = 'unnamedplus',
    -- coc_node_path
    foldmethod = 'manual', -- Commands: zc, zo
    backup = false,
    writebackup = false,
    swapfile = false,
    backupcopy = 'yes',
    backupdir = '',
    tabstop = 4,
    shiftwidth = 4,
    expandtab = true,
    showmatch = true,
    matchpairs = '(:),{:},[:]' .. ',<:>', -- Also, match < and >
    signcolumn = 'yes',
}

vim.g.python3_host_prog = '/usr/bin/python3'
vim.g.airline_powerline_fonts = 1

for k, v in pairs(options) do
    vim.opt[k] = v
end
