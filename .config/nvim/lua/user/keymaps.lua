local opts = { noremap = true, silent = true }

local keymap = vim.api.nvim_set_keymap

-- Modes
--   normal_mode = 'n',
--   insert_mode = 'i',
--   visual_mode = 'v',
--   visual_block_mode = 'x',
--   term_mode = 't',
--   command_mode = 'c',

-- NORMAL
keymap('n', 'k', 'gk', opts)
keymap('n', 'j', 'gj', opts)

keymap('n', '<Leader>w', '<cmd>set wrap!<CR>', opts)

keymap('n', '<Leader>e', '<cmd>NvimTreeFindFileToggle<CR>', opts)

-- TELESCOPE
keymap('n', '<Space>ff', '<cmd>Telescope find_files<CR>', opts)
keymap('n', '<Space>fg', '<cmd>Telescope live_grep<CR>', opts)
keymap('n', '<Space>fb', '<cmd>Telescope buffers<CR>', opts)
keymap('n', '<Space>fh', '<cmd>Telescope help_tags<CR>', opts)

-- VISUAL
keymap('v', 'k', 'gk', opts)
keymap('v', 'j', 'gj', opts)
