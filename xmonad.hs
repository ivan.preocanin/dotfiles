import XMonad
import XMonad.Config.Desktop
import XMonad.Hooks.DynamicLog
import XMonad.Util.EZConfig (additionalKeysP)

import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.EwmhDesktops
import XMonad.Util.Paste
import XMonad.Util.Run(spawnPipe)
import System.IO

import qualified XMonad.StackSet as W

import qualified XMonad.Config as DefConfig

main = do
    xmproc <- spawn "pkill xmobar; xmobar"
    xmonad $ docks def
        { manageHook = manageDocks <+> (isFullscreen --> doFloat) <+> manageHook def
        -- , handleEventHook = fullscreenEventHook
        , layoutHook = avoidStruts $ layoutHook def
        , terminal = "alacritty"
        , borderWidth = 0
        -- , normalBorderColor = "#cccccc"
        -- , focusedBorderColor = "#cd8b00"
        , logHook = dynamicLogWithPP xmobarPP
                        -- { ppOutput = hPutStrLn xmproc
                        -- , ppTitle = xmobarColor "green" "" . shorten 50
                        -- }
                        { ppOutput = \x -> return () } -- This will stop XMonad from writing to pipe
        , modMask = mod4Mask
        } `additionalKeysP` myKeys

---------------
--KEYBINDINGS
---------------
myKeys = [ ("C-$", spawn "setxkbmap -layout rs -variant latin -option caps:swapescape")
         , ("C-`", spawn "setxkbmap -layout us -variant dvp -option caps:swapescape")
         , ("C-<Print>", spawn "sleep 0.2; scrot -s")
         , ("<Print>", spawn "scrot")
         , ("M-&", windows $ W.greedyView "1")
         , ("M-[", windows $ W.greedyView "2")
         , ("M-{", windows $ W.greedyView "3")
         , ("M-}", windows $ W.greedyView "4")
         , ("M-(", windows $ W.greedyView "5")
         , ("M-=", windows $ W.greedyView "6")
         , ("M-*", windows $ W.greedyView "7")
         , ("M-)", windows $ W.greedyView "8")
         , ("M-+", windows $ W.greedyView "9")
         , ("M-S-&", windows $ W.shift "1")
         , ("M-S-[", windows $ W.shift "2")
         , ("M-S-{", windows $ W.shift "3")
         , ("M-S-}", windows $ W.shift "4")
         , ("M-S-(", windows $ W.shift "5")
         , ("M-S-=", windows $ W.shift "6")
         , ("M-S-*", windows $ W.shift "7")
         , ("M-S-)", windows $ W.shift "8")
         , ("M-S-+", windows $ W.shift "9")
         -- , ("M-z", pasteString "ž")
         -- , ("M-S-z", pasteString "Ž")
         ]


