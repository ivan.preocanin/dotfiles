#!/bin/bash
set -euo pipefail
# -e exit on error
# -u no unknown variables
# -o pipefail: Pipes catch errors

while [ $# -gt 0]; do
    if [[ $1 == *"--"* ]]; then
        param="${1/--/}"
        declare $param="$2"
    fi

    shift
done

sed -i '/ParallelDownloads/s/^#//g' /etc/pacman.conf # ParallelDownloads is set to 5 but it's commented out. This uncomments the line
sed -i '/Color/s/^#//g' /etc/pacman.conf

systemctl enable --now NetworkManager.service

nmcli device wifi connect $wifi password $pass # Pass SSID and password as parameters

pacman -S --noconfirm archlinux-keyring

pacman -S --noconfirm --needed  base-devel \ # Needed for makepkg
                                vim nano vi \
                                tmux \
                                man-db man-pages texinfo \
                                glibc \
                                alacritty \
                                zsh zsh-completions \
                                # TODO: Configure zsh. https://wiki.archlinux.org/title/Zsh
                                # Run 'autoload -Uz zsh-newuser-install' or 'zsh-newuser-install -f' to trigger zsh configuration
                                git \
                                ttf-iosevka-nerd ttf-iosevkaterm-nerd xorg-font-util ttf-nerd-fonts-symbols-mono ttf-tibetan-machine \
                                noto-fonts noto-fonts-cjk noto-fonts-extra noto-fonts-emoji ttf-roboto \
                                go \
                                gource \
                                htop calc ncdu newsboat vifm \
                                alsa-utils pipewire pipewire-pulse \
                                libnotify \
                                grim slurp \
                                signal-desktop \
                                discord \
                                transmission-cli \
                                transmission-qt \
                                libreoffice-still \
                                pandoc \
                                pipewire-jack mpv \
                                code \
                                electron \
                                broot \ # Configure
                                docker docker-compose \
                                interception-tools \
                                # This provides uinput command
                                # /dev/input/event8 is touchpad
                                # sudo uinput -p -d /dev/input/event8
                                zathura zathura-cb zathura-djvu zathura-pdf-mupdf \
                                bluez bluez-utils \
                                neovim neovim-nvim-treesitter tree-sitter tree-sitter-cli python python-pip python-pynvim ripgrep fd \
                                light \
                                wl-clipboard \
                                dmenu foot polkit mako sway swaybg swayidle swaylock \
                                unrar unzip \
                                rsync grsync \
                                openssh \
                                intel-ucode \
                                xorg-mkfontdir \
                                inkscape git-lfs texlive-most \
                                youtube-dl \
                                bat \
                                lua-language-server luarocks \
                                lsof \
                                exfat-utils \
                                taskwarrior-tui \
                                spotify-launcher \
                                xorg-xwayland \
                                cups cups-pdf avahi nss-mdns \
                                github-cli glab \
                                torbrowser-launcher \
                                waybar otf-font-awesome \
                                ntfs-3g \
                                imagemagick \
                                ntp \
                                syncthing \
                                cronie \
                                qutebrowser

if id $user &>/dev/null; then
    echo 'User already exists'
else
    useradd -m -s /bin/zsh $user
    usermod -aG wheel,audio,video,docker,seat $user
    passwd $user
fi

# VISUDO

#
# AS NON-ROOT USER
#
amixer sset Master unmute
amixer sset Speaker unmute
amixer sset Headphone unmute

mkdir -p ~/workspace

git clone https://www.gitlab.com/ivan.preocanin/dotfiles.git ~/workspace/dotfiles
chmod +x ~/workspace/dotfiles/setup.sh
./workspace/dotfiles/setup.sh

git clone https://aur.archlinux.org/aura-bin.git
cd aura-bin
makepkg
sudo pacman -U aura*.zst
cd ~
rm -rf aura-bin

gpg --auto-key-locate nodefault,wkd, --locate-keys torbrowser@torproject.org
sudo aura -A zsh-theme-powerlevel10k-git clipman wev google-chrome nvm lagrange nvim-packer-git ltunify swayimg ttf-mononoki nerd-fonts-mononoki elm-platform weather-cli

nvm install --lts

pip install pynvim --upgrade

npm install -g neovim

sudo sed -i '/AutoEnable=/s/^#\s//g' /etc/bluetooth/main.conf
sudo sed -i '/Experimental = false/s/^#//g' /etc/bluetooth/main.conf
sudo sed -i '/Experimental = false/s/false/true/g' /etc/bluetooth/main.conf
systemctl enable --now bluetooth.service

su -c 'modprobe -r pcspkr && echo "blacklist pcspkr" >> /etc/modprobe.d/50-blacklist.conf' # Disable beep on shutdown

sudo systemctl enable --now docker.service

sudo update-mime-database /usr/share/mime # Fix for chrome crashing when opening dialogs
# Copy .vimrc

# Configure BIOS https://wiki.archlinux.org/title/Lenovo_ThinkPad_X1_Carbon_(Gen_6)

# General recommendations:
# https://wiki.archlinux.org/title/General_recommendations#Microcode
# https://wiki.archlinux.org/title/General_recommendations#User_directories
# https://wiki.archlinux.org/title/Solid_state_drive
# https://wiki.archlinux.org/title/Color_output_in_console
# https://wiki.archlinux.org/title/Pacman/Tips_and_tricks

# Touchpad gestures
# Tap with two fingers
# Zoom in or out
# Three fingers up
# Three fingers down
# Three fingers left
# Three fingers right
# Four fingers left
# Four fingers right

# fonts

sudo systemctl enable --now cups.socket
# https://wiki.archlinux.org/title/Avahi#Hostname_resolution
# Then, edit the file /etc/nsswitch.conf and change the hosts line to include mdns_minimal [NOTFOUND=return] before resolve and dns:
# hosts: mymachines mdns_minimal [NOTFOUND=return] resolve [!UNAVAIL=return] files myhostname dns
sudo systemctl enable --now avahi-daemon.service

npm install -g @nestjs/cli

sudo pacman -S prettier

# rustup component add rust-analyzer

sudo aura -A python-validity
# https://github.com/swaywm/swaylock/issues/61#issuecomment-965175390
sudo echo 'auth sufficient pam_unix.so try_first_pass likeauth nullok\n' >> /etc/pam.d/swaylock
sudo echo 'auth sufficient pam_fprintd.so\n' >> /etc/pam.d/swaylock

systemctl enable --now ntpd.service

sudo aura -A postman-bin

systemctl enable --now --user syncthing.service

systemctl enable --now cronie.service
