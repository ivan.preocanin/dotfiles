# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

EDITOR=nvim

# The following lines were added by compinstall

zstyle ':completion:*' completer _complete _ignored
zstyle :compinstall filename '/home/ivan/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
bindkey -v
# End of lines configured by zsh-newuser-install

# Start Sway automatically on TTY login
if [ -z $DISPLAY ] && [ "$(tty)" = "/dev/tty1" ]; then
    exec sway
fi
source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

source /usr/share/nvm/init-nvm.sh

export PATH="/usr/bin/lua-language-server:$PATH"

export MANPAGER=bat

alias chrome='2>/dev/null 1>&2 google-chrome-stable --ozone-platform=wayland & disown'
alias spotify='2>/dev/null 1>&2 spotify-launcher & disown'
alias discord='2>/dev/null 1>&2 discord & disown'
alias vim='nvim'
alias bt='bluetoothctl'
# alias btc='bluetoothctl connect 38:18:4C:6C:EA:B6'
# alias btd='bluetoothctl disconnect 38:18:4C:6C:EA:B6'
alias btc='bluetoothctl cancel-pairing AC:80:0A:03:0A:0B; bluetoothctl pair AC:80:0A:03:0A:0B; bluetoothctl connect AC:80:0A:03:0A:0B'
alias btd='bluetoothctl disconnect AC:80:0A:03:0A:0B'
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias signal='2>/dev/null 1>&2 signal-desktop & disown'
alias za='zathura --fork'
alias dotfiles='cd ~/workspace/dotfiles/'
alias unblock-trackpad="su -c 'printf none > /sys/bus/serio/devices/serio1/drvctl; printf reconnect > /sys/bus/serio/devices/serio1/drvctl'" # https://wiki.archlinux.org/title/Lenovo_ThinkPad_X1_Carbon_(Gen_6)#TrackPoint_and_Touchpad_issues
alias code='code --enable-features=UseOzonePlatform --ozone-platform=wayland'
alias brag='cd ~/workspace/brag-repo/'
alias news='newsboat --refresh-on-start --quiet'
alias print='echo "lpstat -e"; echo "lpr -P printer-name file"'
alias tara='zathura --fork -c "" ~/documents/Dharma/prakse/The\ Profound\ Essence\ Tara.pdf'

source /home/ivan/.config/broot/launcher/bash/br


# Load Angular CLI autocompletion.
source <(ng completion script)
